#pip.main(['install','networkx'])
#pip.main(['install','matplotlib'])
#pip.main(['install','ipython[all]'])
#pip.main(['install','oauth2'])
#pip.main(['install','operator'])
import operator
import pandas
import collections
import pip
import pylab as plt
import networkx as nx
from networkx.algorithms import tree, community
G = nx.read_weighted_edgelist('C:/Users/Vici/Downloads/CE-LC (1).txt', 'rb')

#Drawing the corresponding network G 
plt.axis('off')
plt.title('Network G')
nx.draw_networkx(G,with_labels=False,node_size=50)
plt.show()

#The number of nodes in G
print('The number of nodes in G:', nx.number_of_nodes(G))

#The number of edges in G
print('The number of edges in G:', nx.number_of_edges(G))

#The average degree of the network
print('The average degree of the network G:', nx.average_degree_connectivity(G))

#The density of the network
print ('The density of the network G:', nx.density(G))

#Drawing the minimum tree spanning G
G_mintree = tree.minimum_spanning_tree(G, algorithm='kruskal', weight='mass')
plt.axis('off')
plt.title('Minimum spanning tree')
nx.draw_networkx(G_mintree,with_labels=False,node_size=50)
plt.show()


#Drawing the degree distribution histogram
degree_sequence = sorted([d for n, d in G.degree()], reverse=True) 
degreeCount = collections.Counter(degree_sequence)
deg, cnt = zip(*degreeCount.items())
fig, ax = plt.subplots()
plt.bar(deg, cnt, width=1, color='g')
plt.title("Degree Histogram")
plt.ylabel("Count")
plt.xlabel("Degree")
plt.axes([0.4, 0.4, 0.5, 0.5])
G_DH = sorted(nx.connected_component_subgraphs(G), key=len, reverse=True)[0]
pos = nx.spring_layout(G)
plt.axis('off')
nx.draw_networkx_nodes(G, pos, node_size=50)
nx.draw_networkx_edges(G, pos, alpha=0.4)
plt.show()

#Drawing the graph with the LCC - Largest Connected Component
nx.draw_networkx(G_DH,with_labels=False,node_size=50)

plt.axis('off')
plt.title('LC - Largest Connected Component')
plt.show()

print('Diameter of the LC: ',nx.diameter(G_DH))
print('Center node(s) of the LC: ', nx.center(G_DH))

cliques = nx.find_cliques(G_DH)
cliques3 = [clq for clq in cliques if len(clq) >= 3]
print('Amount of clique communities with 3 or more nodes: ', len(cliques3))

centrality = nx.eigenvector_centrality_numpy(G_DH)

print('Node with the highest importance:' , (max(centrality, key=centrality.get)))


