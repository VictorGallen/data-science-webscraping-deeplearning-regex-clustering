from IPython.display import HTML
import numpy as np
from urllib.request import urlopen
import pip
pip.main(['install','seaborn'])
pip.main(['install','bs4'])
pip.main(['install','lmxl'])
import bs4
import time
import operator
import socket
import pickle
import re
import requests
from pandas import Series
import pandas as pd
from pandas import DataFrame
from lxml import etree
import matplotlib
import matplotlib.pyplot as plt
%matplotlib inline
import json
import seaborn as sns
sns.set_context("talk")
sns.set_style("white")

milestoKM = 1.609344
api_key = 'AIzaSyAbv7WfM7VRfke-FajKVZ9huBp4u1RavPo'

def findDistance(destination):

    origin = 'Helsinki,finland'
    apiUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + origin + '&destinations=' + destination + '&key=' + api_key
    response = requests.get(apiUrl)
    result = response.json()
    dist = round(((float((result['rows'][0]['elements'][0]['distance']['text']).replace(' mi','')))*milestoKM),2)
    return(dist)

def countAvg(string):

    priceList = (string.replace('€','')).split('-')
    try:
        return ((int(priceList[0]) + int(priceList[1])) / 2)
    except:
        list12 = re.findall(r'\d+', string)
        return(int(list12[0]))

    
location = 'Helsinki'
category = 'Breakfast+%26+Brunch'
source = ''
urlList = []
nameList = []
acceptsCreditC = []
avgpriceList = []
reviewCountList = []
distanceFromList = []
openAtWeekends = []
ratingList = []

page = 0

while(location=='Helsinki'):
    url = 'https://www.yelp.com/search?find_desc=' + category + '&find_loc='+ location + ',+Finland&start=' + str(page) + '&sortby=rating'
    page += 10
    source = urlopen(url).read()
    soup = bs4.BeautifulSoup(source)
    if not([link['href'] for link in soup.findAll("a", {"class": "biz-name"})]):
        break
    urlList.extend([link['href'] for link in soup.findAll("a", {"class": "biz-name"})])

for i in range(len(urlList)):
    url = 'https://www.yelp.com/' + urlList[i]
    source = urlopen(url).read()
    soup = bs4.BeautifulSoup(source)
    
    name = ((soup.find("h1", class_="biz-page-title")).contents[0]).strip()
    nameList.append(name)
    
    try:
        ul = soup.find('div', {'class': 'short-def-list'})
        acceptsCredit = (ul.find('dd').contents[0]).strip()
        acceptsCreditC.append(acceptsCredit)
    except:
        acceptsCreditC.append('null')
    
    findPrice = soup.find('dd', {'class': 'price-description'})
    try:
        priceString = (findPrice.contents[0]).strip()
        price = countAvg(priceString)
        avgpriceList.append(price)
    except:
        avgpriceList.append(0)
        
        
    reviewN = soup.find('span', {'class': 'review-count'})
    try:
        
        reviewN1 = (reviewN.contents[0]).strip()
        list12 = re.findall(r'\d+', reviewN1)
        reviewCountList.append(int(list12[0]))
    except:
        reviewCountList.append(0)
        
        
        
    
    try:
        ratingN = (soup.find('div', {'class': 'i-stars'})["title"])
        test = float((ratingN.replace('star rating','')).strip())
        ratingList.append(test)
    
    except:
        ratingList.append(0)
        
    print(ratingList)
    textx = soup.find('span', {'itemprop' : 'streetAddress'})
    distanceFromList.append(findDistance(textx.contents[0] + 'Helsinki'))
    
    try:
        sunOpen = soup.select('.hours-table tbody tr')[6].text.split()
        satOpen = soup.select('.hours-table tbody tr')[5].text.split()
        
        if ('Closed' in sunOpen or 'Closed' in satOpen):
            openAtWeekends.append('Closed')

        else:
            openAtWeekends.append('Open')
            
    except:
        openAtWeekends.append('null')
        

    
    averagePrice = ''
    reviewCount = ''
    distanceFromCentre = ''
    
data_df = {
    'Name' : nameList,
    'Creditcard' : acceptsCreditC,
    'Average_price' : avgpriceList,
    'Review_count' : reviewCountList,
    'Distance_from_centre' : distanceFromList,
    'Open_weekends' : openAtWeekends,
    'Rating' : ratingList
    }

df = pd.DataFrame(data_df)
#print(df) #enable these 2 lines for the figure 2 and 3 shown in the PDF
#print(df[df.d > 20])
df.to_csv('restaurants.csv', sep='|')

import numpy as np
import matplotlib.pyplot as plt
filePd = (pd.read_csv('restaurants.csv', sep="|", encoding='latin-1')).replace('null',np.NaN)

sortedDf = (filePd[(filePd['Review_count'].fillna(0).astype(str).astype(int) > 5) 
& (filePd.Creditcard == 'Yes') 
& (filePd.Average_price.fillna(0).astype(str).astype(float)).between(8,25)
& (filePd.Open_weekends=='Open')]).sort(['Rating'], ascending=[False]) #review count > 5 & creditcard = yes & averagePrice between 8.25 & is open at weekends

ax = sortedDf.head().plot(y=["Review_count",'Average_price','Rating', "Distance_from_centre", ], kind="bar",edgecolor='white')
labels = sortedDf.Name

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
ax.set_xticklabels(labels)
ylabels = ['0','10','20','30','40','+50']
ax.set_yticklabels(ylabels)
ax.set_ylim([0,50])
x=[0,1,2,3,4]
reviews = sortedDf.head().Rating
dist = sortedDf.head().Distance_from_centre

for i,j in zip(x,reviews):
    ax.annotate(str(j) + '*',xy=(i+0.01,j+0.5),fontsize=6)

for i,j in zip(x,dist):
    ax.annotate(str(j) +'km',xy=(i+0.14,j+0.5),fontsize=6)


