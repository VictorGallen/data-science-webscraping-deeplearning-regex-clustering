pip.main(['install','tensorflow'])
pip.main(['install','keras'])
pip.main(['install','sklearn'])
pip.main(['install','seaborn'])
pip.main(['install','keras.models'])


import numpy
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras.optimizers import SGD
import matplotlib.pyplot as plt
import seaborn as sns

seed = 7
numpy.random.seed(seed)

columns = ['CRIM','ZN','INDUS','CHAS','NOX','RM','AGE','DIS','RAD','TAX','PTRATIO','B','LSTAT','MEDV']

#Loading the dataset into Rodeo
dataframe = pd.read_csv("housing.data", delim_whitespace=True,names=columns)
corr = (dataframe.corr(method= 'pearson'))

sns.heatmap(corr, 
        xticklabels=corr.columns,
        yticklabels=corr.columns,
        annot=True)
sns.set(font_scale=1)
std=dataframe.corr(method='pearson')['MEDV']
plt.figure();
std.plot(kind='bar');

dataframe['MEDV'].describe()

dataset = dataframe.values
#Splitting the values
X = dataset[:,0:13]
Y = dataset[:,13]
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=.2)

model = Sequential()
model.add(Dense(26, input_dim=13, kernel_initializer='normal', activation='relu'))
model.add(Dense(13, kernel_initializer='normal'))
model.add(Dense(1, kernel_initializer='normal'))

model.compile(loss='mean_squared_error', optimizer='adam')

model.fit(X_train,Y_train,batch_size=5,epochs=40,validation_data=(X_test,Y_test))


predictions = (model.predict(X_test))
seed = 7
numpy.random.seed(seed)
#Testing the model

#model.fit(X, Y,
#          epochs=20,
#          batch_size=10,
#          validation_split=0.2)
#model.evaluate(X,Y)
#score = model.evaluate(X, Y, batch_size=128)
#print(score)

fig, ax = plt.subplots()
ax.scatter(Y_test, predictions)
ax.plot([Y_test.min(), Y_test.max()], [Y_test.min(), Y_test.max()], 'k--', lw=3)
ax.set_xlabel('Measured')
ax.set_ylabel('Predicted')
print(score)

plt.show()
#plt.plot(predictions)
#plt.plot(Y_test)
#plt.show()
